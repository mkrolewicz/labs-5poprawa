﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Display.Contract;

using Main.Contract;

namespace Main.Implementation
{
    public class MainController : IMainController
    {
        private IDisplay display;

        public MainController(IDisplay display)
        {
            this.display = display;
        }

        public void Method1()
        {
            display.Show("Method_1");
        }

        public void Method2()
        {
            display.Show("Method_2");
        }
    }
}
