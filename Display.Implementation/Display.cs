﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows;

using Display.Contract;
using Lab5.DisplayForm;

namespace Display.Implementation
{
    public class Display : IDisplay
    {
        public void Show(string msg)
        {
            model.Text = msg;
        }
        public DisplayViewModel model = (DisplayViewModel)Application.Current.Dispatcher.Invoke(new Func<DisplayViewModel>(() =>
            {
                var form = new Form();
                var viewModel = new DisplayViewModel();
                form.DataContext = viewModel;
                form.Show();
                return viewModel;
            }), null);
    }
}
