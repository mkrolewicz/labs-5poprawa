﻿using System;
using System.Reflection;

using Main.Contract;
using Main.Implementation;

using Display.Contract;
using Display.Implementation;

using PK.Container;

namespace Lab5.Infrastructure
{
    public struct LabDescriptor
    {
        #region P1

        public static Type Container = typeof(Container);

        #endregion

        #region P2

        public static Assembly MainComponentSpec = Assembly.GetAssembly(typeof(IMainController));
        public static Assembly MainComponentImpl = Assembly.GetAssembly(typeof(MainController));

        public static Assembly DisplayComponentSpec = Assembly.GetAssembly(typeof(IDisplay));
        public static Assembly DisplayComponentImpl = Assembly.GetAssembly(typeof(Display.Implementation.Display));

        #endregion
    }
}
