﻿using System;
using PK.Container;

using Display.Contract;
using Display.Implementation;

using Main.Contract;
using Main.Implementation;

namespace Lab5.Infrastructure
{
    public class Configuration
    {
        /// <summary>
        /// Konfiguruje komponenty używane w aplikacji
        /// </summary>
        /// <returns>Kontener ze zdefiniowanymi komponentami</returns>
        public static IContainer ConfigureApp()
        {
            Container container = new Container();

            container.Register(typeof(Main.Implementation.MainController));
            container.Register(typeof(Display.Implementation.Display));

            return container;
        }
    }
}
